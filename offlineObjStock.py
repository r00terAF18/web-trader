import os
from datetime import datetime
import sqlite3


class Stock:
    def __init__(self, id, name):
        self.ID = id
        self.Name = name
        self.goodToBuy = False
        with open('offlineData/Stock_09_46_35.html', 'r', encoding="utf-8") as f:
            self.apiRes = f.read()
            f.close()

        self.SQLCREATETABEL = f'CREATE TABLE "Tick_{self.ID}" ("ID"	INTEGER NOT NULL, "DateTime"	TEXT, "PL"	INTEGER, "PC"	INTEGER, "PF"	INTEGER, "PY"	INTEGER, "PMin"	INTEGER, "ct_Buy_I_Volume"	NUMERIC, "ct_Sell_N_Volume"	NUMERIC, "ct_Buy_Count_I"	NUMERIC, "ct_Sell_Count_I"	NUMERIC, "ZD"	NUMERIC, "QD"	NUMERIC, "PD"	NUMERIC, "PO"	NUMERIC, "QO"	NUMERIC, "ZO"	NUMERIC, PRIMARY KEY("ID" AUTOINCREMENT))'
        self.SQLCHECKTABEL = f'SELECT count(name) FROM sqlite_master WHERE type="table" AND name="Tick_{self.ID}"'

        self.conn = sqlite3.connect("Databases/StockDB.db")
        self.cur = self.conn.cursor()
        self.cur.execute(self.SQLCHECKTABEL)
        self.conn.commit()

        if self.cur.fetchone()[0] == 1:
            pass
        else:
            self.cur.execute(self.SQLCREATETABEL)
            self.conn.commit()

        self.fullResponseList = []
        self.preTabelVals = []
        self.tabelVals = []
        self.rowItems = {}
        self.imp = []
        self.DATA_X = []
        self.DATA_Y = []
        self.DATA_QO = []

        for item in self.apiRes.split(','):
            self.fullResponseList.append(item)

        for item in self.apiRes.split(';'):
            self.preTabelVals.append(item)

        self.preTabelVals.pop()

        for item in self.preTabelVals[2].split(','):
            self.tabelVals.append(item)

        self.tabelVals.pop()

        for item in self.preTabelVals[4].split(','):
            self.imp.append(item)

        try:
            for i in range(len(self.tabelVals)):
                zd = self.tabelVals[i].split('@')[0]
                qd = self.tabelVals[i].split('@')[1]
                pd = self.tabelVals[i].split('@')[2]
                po = self.tabelVals[i].split('@')[3]
                qo = self.tabelVals[i].split('@')[4]
                zo = self.tabelVals[i].split('@')[5]
                date = datetime.now()
                data = f'{zd}_{qd}_{pd}_{po}_{qo}_{zo}'
                self.cur.execute(
                    f'INSERT INTO Tick_{self.ID}("DateTime", "Data") VALUES(?, ?)', (date, data, ))
                self.rowItems[i] = data
        except IndexError:
            # print('encountered Index Error')
            pass
        finally:
            pass

        self.conn.close()

        try:
            self.pl = int(self.fullResponseList[2])
            self.pc = int(self.fullResponseList[3])
            self.pf = int(self.fullResponseList[4])
            self.py = int(self.fullResponseList[5])
            self.pmin = int(self.fullResponseList[6])
            self.tno = int(self.fullResponseList[8])
            self.tvol = int(self.fullResponseList[9])
            self.tval = float(self.fullResponseList[10])

            if len(self.imp) > 2:
                self.ct_Buy_I_Volume = int(self.imp[0])
                self.ct_Sell_N_Volume = int(self.imp[3])
                self.ct_Buy_Count_I = int(self.imp[5])
                self.ct_Sell_Count_I = int(self.imp[8])
        except ValueError or IndexError as ie:
            print(f'Encountered Value Error {ie.__cause__}')
            pass
        finally:
            pass

        self.checkIfGood()

        if len(self.imp) > 2:
            self._calcPP()
        else:
            pass

        # self.fullResponseList.clear()
        # self.preTabelVals.clear()
        # self.tabelVals.clear()

        self.checkIfGood()

    def checkIfGood(self):
        if self.pl == self.pmin:
            self.goodToBuy = True
            return True
        else:
            return False

    def _calcPP(self):
        if self.goodToBuy:
            self.PP = ((self.ct_Buy_I_Volume / self.ct_Buy_Count_I) /
                       (self.ct_Sell_N_Volume / self.ct_Sell_Count_I))
            return self.PP
        else:
            pass

    def getDATA(self):
        cur.execute(f'SELECT * FROM Tick_{self.ID}')
        data = cur.fetchall()

        sort1 = []
        for item in data:
            itemForSort = item[2].split(',')[1]
            sort1.append(itemForSort.replace(' ', ''))
            # print(f'Item from sort1 => {sort1}')

        for item in sort1:
            self.DATA_X.append(item.split('_')[4])

        for i in range(len(self.DATA_X)):
            if i == 0:
                self.DATA_QO.append(float(1))
            else:
                qo1 = float(DATA_X[i])
                qo2 = float(DATA_X[i - 1])
                qo = qo1 / qo2
                self.DATA_QO.append(float(qo))

        for item in data:
            self.DATA_Y.append(item[2].replace('.', '-'))


if __name__ == "__main__":
    pass
