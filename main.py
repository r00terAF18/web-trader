try:
    import os
    import sqlite3
    import time
    from ObjStock import Stock
except ImportError or ImportWarning as ie:
    print(
        f'[-] There were some errors/warnings with imports => {ie.__cause__}')
    exit()


def cls():
    os.system('cls' if os.name == 'nt' else 'clear')


try:
    # conn = sqlite3.connect("Databases/TicksDB.db")
    # cur = conn.cursor()

    # listOfStocks = []

    # dataFromTabel = cur.execute("SELECT * FROM Ticks")

    # i = 1
    # for id in dataFromTabel:
    #     st = Stock(id[0], id[2])
    #     if st.checkIfGood():
    #         listOfStocks.append(st)
    #         print(f'{st.Name} is Good! ({i})')
    #         i += 1

    # cur.close()
    # conn.close()

    stID = input("ID for Stock => ")
    st = Stock(stID)
    cls()

    while True:
        st.askAPI()
        st.fillLists()
        st.Info()
        st.insertData()
        time.sleep(3)
        cls()
        # for st in listOfStocks:
        #     st._getDATA()
        #     if len(st.DATA_QO) == len(st.DATA_Y):
        #         plt.plot(st.DATA_Y, st.DATA_QO, marker='o',
        #                 linestyle='solid', label=f'QO {str(st.Name)[::-1]}')
        #         plt.xlabel('Time')
        #         plt.ylabel('QO')
        #         plt.title('QO calculated on Stock basis')
        #         plt.gcf().autofmt_xdate()
        #         plt.legend()
        #         plt.grid()
        #     else:
        #         pass

        # # plt.tight_layout()
        # plt.show()

        # st._getDATA()
        # st.calcQO()


except KeyboardInterrupt:
    print("\n[-] User interrupted programm!")
