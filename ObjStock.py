import os
import requests
from datetime import datetime
import sqlite3


class Stock:
    def __init__(self, id):
        self.ID = id

        self.fullResponseList = []
        self.preTabelVals = []
        self.tabelVals = []
        self.rowItems = {}
        self.imp = []

        self.DATA_X = []
        self.DATA_Y = []
        self.DATA_QO = []

        self._openDB()
        self._checkDB()
        self._closeDB()

        # if len(self.DATA_X) != 0:
        #     self.calcQO()
        # else:
        #     pass

    def insertData(self):
        date = datetime.now()
        self._openDB()
        self.cur.execute(
            f'INSERT INTO Tick_{self.ID}("DateTime", "PL", "PC", "PF", "PY", "PMin", "ct_Buy_I_Volume", "ct_Sell_N_Volume", "ct_Buy_Count_I", "ct_Sell_Count_I", "ZD", "QD", "PD", "PO", "QO", "ZO") VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', (date, self.pl, self.pc, self.pf, self.py, self.pmin, self.ct_Buy_I_Volume, self.ct_Sell_N_Volume, self.ct_Buy_Count_I, self.ct_Sell_Count_I, self.zd, self.qd, self.pd, self.po, self.qo, self.zo, ))
        self.conn.commit()
        self._closeDB()
        self.pl = 0
        self.pc = 0
        self.pd = 0
        self.pf = 0
        self.po = 0
        self.pmin = 0
        self.py = 0
        self.zd = 0
        self.zo = 0
        self.qd = 0
        self.qo = 0
        self.ct_Buy_I_Volume = 0
        self.ct_Sell_N_Volume = 0
        self.ct_Buy_Count_I = 0
        self.ct_Sell_Count_I = 0


    def fillLists(self):  # Function fills in list with the saved API Response
        try:
            for item in self.apiRes.split(','):
                self.fullResponseList.append(item)
        except IndexError as ie:
            # print(f'[-] Encountered Index Error => {ie.__cause__}')
            pass
        finally:
            pass

        try:
            for item in self.apiRes.split(';'):
                self.preTabelVals.append(item)
        except IndexError as ie:
            # print(f'[-] Encountered Index Error => {ie.__cause__}')
            pass
        finally:
            pass

        self.preTabelVals.pop()

        try:
            for item in self.preTabelVals[2].split(','):
                self.tabelVals.append(item)
        except IndexError as ie:
            # print(f'[-] Encountered Index Error => {ie.__cause__}')
            pass
        finally:
            pass

        try:
            for item in self.preTabelVals[4].split(','):
                self.imp.append(item)
        except IndexError as ie:
            # print(f'[-] Encountered Index Error => {ie.__cause__}')
            pass
        finally:
            pass

        try:
            if len(self.fullResponseList) > 2:
                self.pl = int(self.fullResponseList[2])
                self.pc = int(self.fullResponseList[3])
                self.pf = int(self.fullResponseList[4])
                self.py = int(self.fullResponseList[5])
                self.pmin = int(self.fullResponseList[6])
                self.tno = int(self.fullResponseList[8])
                self.tvol = int(self.fullResponseList[9])
                self.tval = float(self.fullResponseList[10])
            else:
                self.pl = int(0)
                self.pc = int(0)
                self.pf = int(0)
                self.py = int(0)
                self.pmin = int(0)
                self.tno = int(0)
                self.tvol = int(0)
                self.tval = float(0)

            if len(self.imp) > 2:
                self.ct_Buy_I_Volume = int(self.imp[0])
                self.ct_Sell_N_Volume = int(self.imp[3])
                self.ct_Buy_Count_I = int(self.imp[5])
                self.ct_Sell_Count_I = int(self.imp[8])
            else:
                self.ct_Buy_I_Volume = int(0)
                self.ct_Sell_N_Volume = int(0)
                self.ct_Buy_Count_I = int(0)
                self.ct_Sell_Count_I = int(0)
        except ValueError or IndexError as ie:
            # print(f'[-] Encountered Value/Index Error => {ie.__cause__}')
            pass
        finally:
            pass

        try:
            self.tabelVals.pop()
            self.zd = float(0)
            self.qd = float(0)
            self.pd = float(0)
            self.po = float(0)
            self.qo = float(0)
            self.zo = float(0)
            # for i in range(len(self.tabelVals)):
            #     zd = float(0)
            #     qd = float(0)
            #     pd = float(0)
            #     po = float(0)
            #     qo = float(0)
            #     zo = float(0)

            #     zd += float(self.tabelVals[i].split('@')[0])
            #     qd += float(self.tabelVals[i].split('@')[1])
            #     pd += float(self.tabelVals[i].split('@')[2])
            #     po += float(self.tabelVals[i].split('@')[3])
            #     qo += float(self.tabelVals[i].split('@')[4])
            #     zo += float(self.tabelVals[i].split('@')[5])

            #     self.zd += zd
            #     self.qd += qd
            #     self.pd += pd
            #     self.po += po
            #     self.qo += qo
            #     self.zo += zo

            self.zd = float(self.tabelVals[0].split('@')[0]) # self.zd / len(self.tabelVals)
            self.qd = float(self.tabelVals[0].split('@')[1]) # self.qd / len(self.tabelVals)
            self.pd = float(self.tabelVals[0].split('@')[2])
            self.po = float(self.tabelVals[0].split('@')[3])
            self.qo = float(self.tabelVals[0].split('@')[4])
            self.zo = float(self.tabelVals[0].split('@')[5])
        except IndexError as ie:
            # print(f'[-] Encountered Index Error => {ie.__cause__}')
            pass
        finally:
            pass

        self.apiRes = ''
        self.fullResponseList.clear()
        self.preTabelVals.clear()
        self.tabelVals.clear()
        self.imp.clear()

    def askAPI(self):  # Function sends and saves a request for the approprieate data
        self.baseURL = f'http://www.tsetmc.com/tsev2/data/instinfodata.aspx?i={str(self.ID)}&c=72+'
        req = requests.get(self.baseURL)
        self.apiRes = req.content.decode("utf-8")

    def _checkDB(self):  # Function checks the database to see if a tabel with this ID exists
        # If yes, no tabel is created, and data is added to the old on
        # if no tabels were found, a new one is created
        SQLCREATETABEL = f'CREATE TABLE "Tick_{self.ID}" ("ID"	INTEGER NOT NULL, "DateTime"	TEXT, "PL"	INTEGER, "PC"	INTEGER, "PF"	INTEGER, "PY"	INTEGER, "PMin"	INTEGER, "ct_Buy_I_Volume"	NUMERIC, "ct_Sell_N_Volume"	NUMERIC, "ct_Buy_Count_I"	NUMERIC, "ct_Sell_Count_I"	NUMERIC, "ZD"	NUMERIC, "QD"	NUMERIC, "PD"	NUMERIC, "PO"	NUMERIC, "QO"	NUMERIC, "ZO"	NUMERIC, PRIMARY KEY("ID" AUTOINCREMENT));'
        SQLCHECKTABEL = f'SELECT count(name) FROM sqlite_master WHERE type="table" AND name="Tick_{self.ID}"'

        self.cur.execute(SQLCHECKTABEL)
        self.conn.commit()

        if self.cur.fetchone()[0] == 1:
            pass
        else:
            self.cur.execute(SQLCREATETABEL)
            self.conn.commit()

    def _openDB(self):
        self.conn = sqlite3.connect(f'Databases/Stock_{self.ID}.db')
        self.cur = self.conn.cursor()

    def _closeDB(self):
        self.cur.close()
        self.conn.close()

    def checkIfGood(self):
        if len(self.fullResponseList) > 2:
            if self.pl == self.pmin:
                return True
            else:
                return False
        else:
            return False

    def _calcPP(self):
        if self.checkIfGood() and len(self.imp) > 2:
            self.PP = ((self.ct_Buy_I_Volume / self.ct_Buy_Count_I) /
                       (self.ct_Sell_N_Volume / self.ct_Sell_Count_I))
            # return self.PP
        else:
            pass

    def calcQO(self):
        for i in range(len(self.DATA_X)):
            if i == 0:
                self.DATA_QO.append(self.DATA_X[0])
                print(f'calcQO i == 0 => {self.DATA_X[0]}')
            elif i >= 1:
                qo1 = float(self.DATA_X[i])
                qo2 = float(self.DATA_X[i - 1])
                if qo2 != float(0):
                    print(f'_getDATA qo1 => {qo1} || qo2 => {qo2}')
                    qo = qo1 / qo2
                    self.DATA_QO.append(float(qo))
                else:
                    pass
            else:
                self.DATA_QO.append(self.DATA_X[0])

    def _getDATA(self):
        self._openDB()
        self.cur.execute(f'SELECT * FROM Tick_{self.ID}')
        self.data = self.cur.fetchall()

        for item in self.data:
            # if ',' in item:
            # itemForSort = item[2].split('_')[1]
            itemForSort = item[2].split(',')[1]
            forDATAX = itemForSort.replace(' ', '')
            self.DATA_X.append(forDATAX.split('_')[4])

        for i in range(len(self.DATA_X)):
            if i == 0:
                self.DATA_QO.append(float(1))
            else:  # elif  i >= 1
                qo1 = float(self.DATA_X[i])
                qo2 = float(self.DATA_X[int(i - 1)])
                if qo2 != float(0):
                    qo = qo1 / qo2
                    self.DATA_QO.append(float(qo))
                else:
                    pass

        for item in self.data:
            self.DATA_Y.append(item[1])

        self._closeDB()
        # if len(self.DATA_QO) > len(self.DATA_Y):
        #     self.DATA_QO.pop(0)
        # else:
        #     pass

    def Info(self):
        print(f'ID => {self.ID}')
        print(
            f'Time => {datetime.now().hour}:{datetime.now().minute}:{datetime.now().second}')
        print(f'PL => {self.pl}')
        print(f'PMin => {self.pmin}')


if __name__ == "__main__":
    pass
